if [ "$1" != "" ]; then
  echo "compiling lilypond file $(basename $1)/$(basename $1).ly (output pdf, png and midi files)"
	lilypond --pdf --png -o $1 $(basename $1)/$(basename $1).ly;
	echo "convert $(basename $1)/$(basename $1).midi file to $(basename $1)/$(basename $1).ogg"
	timidity $(basename $1)/$(basename $1).midi -Ov -o $(basename $1)/$(basename $1).ogg
fi

