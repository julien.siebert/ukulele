if [ "$1" != "" ]; then
  echo "removing pdf, png, midi, and ogg files in $1"
  rm -I $(basename $1)/$(basename $1).png;
  rm -I $(basename $1)/$(basename $1).pdf;
  rm -I $(basename $1)/$(basename $1).midi;
  rm -I $(basename $1)/$(basename $1).ogg;    
fi

