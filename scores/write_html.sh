#!/bin/bash
write_page()
{
    echo "
    <html>
        <head>
        <title>$title</title>
        </head>
        <body>
        <div>
        <audio controls>
          <source src=\"./$title.ogg\" type=\"audio/ogg\">
          <source src=\"./$title.midi\" type=\"audio/midi\">
        Your browser does not support the audio element.
        </audio> 
        </div>
        <div>
          <ul>
            <li><a href=\"./$title.pdf\">pdf version</a></li>
            <li><a href=\"./$title.tg\">tux guitar version</a></li>
            <li><a href=\"./$title.ly\">lilypond version</a></li>
            <li><a href=\"./$title.midi\">midi version</a></li>
          </ul>
        </div>
        <div>"
    for f in $(ls $title/*.png);
    do
      echo "
          <img src=\"./$(basename $f)\">"
    done
    echo "
        </div>
        </body>
    </html>"
}

usage()
{
    echo "usage: $0 [[[-f file ]] | [-h]]"
}


##### Main


while [ "$1" != "" ]; do
    case $1 in
        -f | --file )           shift
                                title=$(basename $1)
                                filename=$(basename $1)/$(basename $1).html
                                ;;
        -h | --help )           usage
                                exit
                                ;;
        * )                     usage
                                exit 1
    esac
    shift
done


# Test code to verify command line processing

echo "output file = $filename"


# Write page (comment out until testing is complete)

write_page > $filename
