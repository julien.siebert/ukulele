#(define (tie::tab-clear-tied-fret-numbers grob)
   (let* ((tied-fret-nr (ly:spanner-bound grob RIGHT)))
      (ly:grob-set-property! tied-fret-nr 'transparent #t)))

\version "2.16.0"
\paper {
   indent = #0
   print-all-headers = ##t
   print-all-headers = ##t
   ragged-right = ##f
   ragged-bottom = ##t
}
\layout {
   \context { \Score
      \override MetronomeMark #'padding = #'5
   }
   \context { \Staff
      \override TimeSignature #'style = #'numbered
      \override StringNumber #'transparent = ##t
   }
   \context { \TabStaff
      \override TimeSignature #'style = #'numbered
      \override Stem #'transparent = ##t
      \override Flag #'transparent = ##t
      \override Beam #'transparent = ##t
      \override Tie  #'after-line-breaking = #tie::tab-clear-tied-fret-numbers
   }
   \context { \TabVoice
      \override Tie #'stencil = ##f
   }
   \context { \StaffGroup
      \consists "Instrument_name_engraver"
   }
}
deadNote = #(define-music-function (parser location note) (ly:music?)
   (set! (ly:music-property note 'tweaks)
      (acons 'stencil ly:note-head::print
         (acons 'glyph-name "2cross"
            (acons 'style 'special
               (ly:music-property note 'tweaks)))))
   note)

palmMute = #(define-music-function (parser location note) (ly:music?)
   (set! (ly:music-property note 'tweaks)
      (acons 'style 'do (ly:music-property note 'tweaks)))
   note)

TrackAVoiceAMusic = #(define-music-function (parser location inTab) (boolean?)
#{

   \clef #(if inTab "tab" "treble_8")
   \key g \major
   \time 4/4
   \oneVoice
   <d\3 g\2 >8 <g\4>8 <g\2 e\3 >8 <a\1>8 <d\3 g\2 >4 <d\3>4
   <d\3 g\2 >8 <g\4>8 <g\2 e\3 >8 <a\1>8 <d\3 g\2 >4 <d\3>4
   <c'\1 g\4 >8 <b\1>8 <a\1 e\2 >8 <b\1>8 <c'\1 g\4 >8 <b\1>8 <a\1 e\2 >4
   <c'\1 a\4 >8 <b\1>8 <a\1 fis\2 >8 <b\1>8 <c'\1 a\4 >8 <b\1>8 <a\1 fis\2 >4
   <d\3 g\2 >8 <g\4>8 <g\2 e\3 >8 <a\1>8 <d\3 g\2 >4 <d\3>4
   <d\3 g\2 >8 <g\4>8 <g\2 e\3 >8 <a\1>8 <d\3 g\2 >4 <d\3>4
   <d\3 g\2 >8 <g\4>8 <g\2 e\3 >8 <g\4>8 <e\2 a\1 >4 <g\4 e\2 >8 <g\4 e\2 >8
   <a\1 e\2 >8 <g\4 e\2 >8 <e\2 a\1 >8 <g\4 e\2 >8 <e\2 a\1 >4 <fis\2 d'\1 >4
   <g\4 d\3 g\2 >1
   \bar "|."
   \pageBreak
#})
TrackAVoiceBMusic = #(define-music-function (parser location inTab) (boolean?)
#{
#})
TrackALyrics = \lyricmode {
   \set ignoreMelismata = ##t

   \unset ignoreMelismata
}
TrackAStaff = \new Staff <<
   \context Voice = "TrackAVoiceAMusic" {
      \TrackAVoiceAMusic ##f
   }
   \context Voice = "TrackAVoiceBMusic" {
      \TrackAVoiceBMusic ##f
   }
>>
TrackATabStaff = \new TabStaff \with { stringTunings = #`(,(ly:make-pitch -1 5 NATURAL) ,(ly:make-pitch -1 2 NATURAL) ,(ly:make-pitch -1 0 NATURAL) ,(ly:make-pitch -1 4 NATURAL) ) } <<
   \context TabVoice = "TrackAVoiceAMusic" {
      \removeWithTag #'chords
      \removeWithTag #'texts
      \TrackAVoiceAMusic ##t
   }
   \context TabVoice = "TrackAVoiceBMusic" {
      \removeWithTag #'chords
      \removeWithTag #'texts
      \TrackAVoiceBMusic ##t
   }
>>
TrackAStaffGroup = \new StaffGroup <<
   \TrackAStaff
   \TrackATabStaff
>>
\score {
   \TrackAStaffGroup

   \header {
      title = "Une Souris Verte"
      %composer = "Anon."
      copyright="CC BY-SA 4.0"
      instrument = "Ukulele (gCEA)"
      poet="Arranged by Julien Siebert"
   }
\layout{}\midi{}
}
