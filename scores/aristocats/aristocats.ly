#(define (tie::tab-clear-tied-fret-numbers grob)
(let* ((tied-fret-nr (ly:spanner-bound grob RIGHT)))
(ly:grob-set-property! tied-fret-nr 'transparent #t)))

\version "2.16.0"

custom-tuning = \stringTuning <g c e a>

\paper {
  indent = #0
  print-all-headers = ##t
  print-all-headers = ##t
  ragged-right = ##f
  ragged-bottom = ##t
}
\layout {
  \context { \Score
    \override MetronomeMark #'padding = #'5
    proportionalNotationDuration = #(ly:make-moment 1/16)
  }
  \context { \Staff
    \override TimeSignature #'style = #'numbered
    \override StringNumber #'transparent = ##t
  }
  \context { \TabStaff
    \override TimeSignature #'style = #'numbered
    \override Stem #'transparent = ##t
    \override Flag #'transparent = ##t
    \override Beam #'transparent = ##t
    \override Tie  #'after-line-breaking = #tie::tab-clear-tied-fret-numbers
  }
  \context { \TabVoice
    \override Tie #'stencil = ##f
  }
  \context { \StaffGroup
    \consists "Instrument_name_engraver"
  }
}
deadNote = #(define-music-function (parser location note) (ly:music?)
(set! (ly:music-property note 'tweaks)
(acons 'stencil ly:note-head::print
(acons 'glyph-name "2cross"
(acons 'style 'special
(ly:music-property note 'tweaks)))))
note)

palmMute = #(define-music-function (parser location note) (ly:music?)
(set! (ly:music-property note 'tweaks)
(acons 'style 'do (ly:music-property note 'tweaks)))
note)

TrackAVoiceAMusic = #(define-music-function (parser location inTab) (boolean?)
#{
  %\tempo 4=120
  \clef #(if inTab "tab" "treble_8")
  \key c \major
  \time 4/4
  \oneVoice
  <e\3 g\2 b\1 g\4 >4
  -\tag #'texts ^\markup {"d"}
  -\tag #'chords ^\markup \fret-diagram #"w:4;h:4;1-2;2-3;3-4;4-x;"
  <b\1 g\2 e\3 g\4 >8
  -\tag #'texts ^\markup {"d"}
  <e\3 g\2 b\1 g\4 >8
  -\tag #'texts ^\markup {"u"}
  <dis\3 g\2 b\1 g\4 >4
  -\tag #'texts ^\markup {"d"}
  -\tag #'chords ^\markup \fret-diagram #"w:4;h:4;1-2;2-3;3-3;4-x;"
  <b\1 g\2 dis\3 g\4 >8
  -\tag #'texts ^\markup {"d"}
  <dis\3 g\2 b\1 g\4 >8
  -\tag #'texts ^\markup {"u"}
  <d\3 g\2 b\1 g\4 >4
  -\tag #'texts ^\markup {"d"}
  -\tag #'chords ^\markup \fret-diagram #"w:4;h:4;1-2;2-3;3-2;4-x;"
  <d\3 g\2 b\1 g\4 >8
  -\tag #'texts ^\markup {"d"}
  <d\3 g\2 b\1 g\4 >8
  -\tag #'texts ^\markup {"u"}
  <cis\3 g\2 b\1 g\4 >4
  -\tag #'texts ^\markup {"d"}
  -\tag #'chords ^\markup \fret-diagram #"w:4;h:4;1-2;2-3;3-1;4-x;"
  <cis\3 g\2 b\1 g\4 >8
  -\tag #'texts ^\markup {"d"}
  <cis\3 g\2 b\1 g\4 >8
  -\tag #'texts ^\markup {"u"}
  <c\3 e\2 b\1 g\4 >4
  -\tag #'texts ^\markup {"d"}
  -\tag #'chords ^\markup \fret-diagram #"w:4;h:4;1-2;2-o;3-o;4-o;"
  <c\3 e\2 b\1 g\4 >8
  -\tag #'texts ^\markup {"d"}
  <a~\4 c~\3 fis~\2 a~\1 >8
  -\tag #'texts ^\markup {"u"}
  -\tag #'chords ^\markup \fret-diagram #"w:4;h:4;1-o;2-2;3-o;4-2;"
  <a\4 c\3 fis\2  >8
  <a\4 c\3 fis\2 a\1 >8
  -\tag #'texts ^\markup {"u"}
  <a\4 c\3 fis\2 a\1 >4
  -\tag #'texts ^\markup {"d"}
  <ais\1 e\2 c\3 g\4 >8
  -\tag #'texts ^\markup {"d"}
  -\tag #'chords ^\markup \fret-diagram #"w:4;h:4;1-1;2-o;3-o;4-o;"
  <g\4 c\3 e\2 ais\1 >8
  -\tag #'texts ^\markup {"u"}
  <\deadNote ais\1 \deadNote e\2 \deadNote c\3 \deadNote g\4 >8
  -\tag #'texts ^\markup {"d"}
  <a\4 fis\2 b\1 dis\3 >8
  -\tag #'texts ^\markup {"u"}
  -\tag #'chords ^\markup \fret-diagram #"w:4;h:4;1-2;2-2;3-3;4-2;"
  r8
  <a\4 fis\2 b\1 dis\3 >8
  -\tag #'texts ^\markup {"u"}
  <a\4 dis\3 fis\2 b\1 >4
  -\tag #'texts ^\markup {"d"}
  <e\3 g\2 b\1 g\4 >4
  -\tag #'texts ^\markup {"d"}
  -\tag #'chords ^\markup \fret-diagram #"w:4;h:4;1-2;2-3;3-4;4-x;"
  <b\1 g\2 e\3 g\4 >8
  -\tag #'texts ^\markup {"d"}
  <e\3 g\2 b\1 g\4 >8
  -\tag #'texts ^\markup {"u"}
  <dis\3 g\2 b\1 g\4 >4
  -\tag #'texts ^\markup {"d"}
  -\tag #'chords ^\markup \fret-diagram #"w:4;h:4;1-2;2-3;3-3;4-x;"
  <b\1 g\2 dis\3 g\4 >8
  -\tag #'texts ^\markup {"d"}
  <dis\3 g\2 b\1 g\4 >8
  -\tag #'texts ^\markup {"u"}
  <d\3 g\2 b\1 g\4 >4
  -\tag #'texts ^\markup {"d"}
  -\tag #'chords ^\markup \fret-diagram #"w:4;h:4;1-2;2-3;3-2;4-x;"
  <d\3 g\2 b\1 g\4 >8
  -\tag #'texts ^\markup {"d"}
  <d\3 g\2 b\1 g\4 >8
  -\tag #'texts ^\markup {"u"}
  <cis\3 g\2 b\1 g\4 >4
  -\tag #'texts ^\markup {"d"}
  -\tag #'chords ^\markup \fret-diagram #"w:4;h:4;1-2;2-3;3-1;4-x;"
  <cis\3 g\2 b\1 g\4 >8
  -\tag #'texts ^\markup {"d"}
  <cis\3 g\2 b\1 g\4 >8
  -\tag #'texts ^\markup {"u"}
  <c\3 e\2 b\1 g\4 >4
  -\tag #'texts ^\markup {"d"}
  -\tag #'chords ^\markup \fret-diagram #"w:4;h:4;1-2;2-o;3-o;4-o;"
  <c\3 e\2 b\1 g\4 >8
  -\tag #'texts ^\markup {"d"}
  <g\4 c\3 e\2 b\1 >8
  -\tag #'texts ^\markup {"u"}
  <a\4 dis\3 fis\2 b\1 >8
  -\tag #'texts ^\markup {"d"}
  -\tag #'chords ^\markup \fret-diagram #"w:4;h:4;1-2;2-2;3-3;4-2;"
  <a\4 dis\3 fis\2 b\1 >8
  -\tag #'texts ^\markup {"u"}
  <\deadNote a\4 \deadNote c\3 \deadNote fis\2 \deadNote a\1 >8
  -\tag #'texts ^\markup {"d"}
  <e~\3 g~\2 b~\1 >8
  -\tag #'texts ^\markup {"u"}
  -\tag #'chords ^\markup \fret-diagram #"w:4;h:4;1-2;2-3;3-4;4-x;"
  <b\1 g\2 e\3 >8
  <b\1 g\2 e\3 g\4 >8
  -\tag #'texts ^\markup {"u"}
  <b\1 g\2 e\3 g\4 >8
  -\tag #'texts ^\markup {"d"}
  <b~\1 g~\2 e~\3 g~\4 >8
  -\tag #'texts ^\markup {"u"}
  <b\1 g\2 e\3  >8
  <b\1 g\2 e\3 g\4 >8
  -\tag #'texts ^\markup {"u"}
  <b\1 g\2 e\3 g\4 >4
  -\tag #'texts ^\markup {"d"}
  <a\4 c\3 e\2 a\1 >4
  -\tag #'texts ^\markup {"d"}
  -\tag #'chords ^\markup \fret-diagram #"w:4;h:4;1-o;2-o;3-o;4-2;"
  <a\4 a\1 e\2 c\3 >8
  -\tag #'texts ^\markup {"d"}
  <a\4 c\3 e\2 a\1 >8
  -\tag #'texts ^\markup {"u"}
  <gis\4 c\3 e\2 a\1 >4
  -\tag #'texts ^\markup {"d"}
  -\tag #'chords ^\markup \fret-diagram #"w:4;h:4;1-o;2-o;3-o;4-1;"
  <gis\4 c\3 e\2 a\1 >8
  -\tag #'texts ^\markup {"d"}
  <gis\4 c\3 e\2 a\1 >8
  -\tag #'texts ^\markup {"u"}
  <g\4 c\3 e\2 a\1 >4
  -\tag #'texts ^\markup {"d"}
  -\tag #'chords ^\markup \fret-diagram #"w:4;h:4;1-o;2-o;3-o;4-o;"
  <a\1 e\2 c\3 g\4 >8
  -\tag #'texts ^\markup {"d"}
  <g\4 c\3 e\2 a\1 >8
  -\tag #'texts ^\markup {"u"}
  <a\4 c\3 fis\2 a\1 >4
  -\tag #'texts ^\markup {"d"}
  -\tag #'chords ^\markup \fret-diagram #"w:4;h:4;1-o;2-2;3-o;4-2;"
  <a\1 fis\2 c\3 a\4 >8
  -\tag #'texts ^\markup {"d"}
  <a\4 c\3 fis\2 a\1 >8
  -\tag #'texts ^\markup {"u"}
  <g\4 d\3 g\2 b\1 >4
  -\tag #'texts ^\markup {"d"}
  -\tag #'chords ^\markup \fret-diagram #"w:4;h:4;1-2;2-3;3-2;4-o;"
  <b\1 g\2 d\3 g\4 >4
  -\tag #'texts ^\markup {"d"}
  <b\1 g\2 d\3 g\4 >4
  -\tag #'texts ^\markup {"d"}
  <b\1 g\2 d\3 g\4 >8
  -\tag #'texts ^\markup {"d"}
  <g\4 d\3 g\2 b\1 >8
  -\tag #'texts ^\markup {"u"}
  <b\1 f\2 d\3 g\4 >8
  -\tag #'texts ^\markup {"d"}
  -\tag #'chords ^\markup \fret-diagram #"w:4;h:4;1-2;2-1;3-2;4-o;"
  <b\1 f\2 d\3 g\4 >8
  -\tag #'texts ^\markup {"u"}
  <b\1 f\2 d\3 g\4 >8
  -\tag #'texts ^\markup {"d"}
  <b\1 f\2 d\3 g\4 >8
  -\tag #'texts ^\markup {"u"}
  r8
  <b\1 f\2 d\3 g\4 >8
  -\tag #'texts ^\markup {"u"}
  <b\1 f\2 d\3 g\4 >4
  -\tag #'texts ^\markup {"d"}
  <a\4 c\3 e\2 a\1 >4
  -\tag #'texts ^\markup {"d"}
  -\tag #'chords ^\markup \fret-diagram #"w:4;h:4;1-o;2-o;3-o;4-2;"
  <\deadNote a\1 \deadNote e\2 \deadNote c\3 \deadNote g\4 >8
  -\tag #'texts ^\markup {"d"}
  <a\4 dis\3 fis\2 b\1 >8
  -\tag #'texts ^\markup {"u"}
  -\tag #'chords ^\markup \fret-diagram #"w:4;h:4;1-2;2-2;3-3;4-2;"
  r8
  <a\4 dis\3 fis\2 b\1 >8
  -\tag #'texts ^\markup {"u"}
  <b\1 fis\2 dis\3 a\4 >4
  -\tag #'texts ^\markup {"d"}
  <a\4 c\3 e\2 a\1 >4
  -\tag #'texts ^\markup {"d"}
  -\tag #'chords ^\markup \fret-diagram #"w:4;h:4;1-o;2-o;3-o;4-2;"
  <\deadNote a\1 \deadNote e\2 \deadNote c\3 \deadNote g\4 >8
  -\tag #'texts ^\markup {"d"}
  <a\4 dis\3 fis\2 b\1 >8
  -\tag #'texts ^\markup {"u"}
  -\tag #'chords ^\markup \fret-diagram #"w:4;h:4;1-2;2-2;3-3;4-2;"
  r8
  <a\4 dis\3 fis\2 b\1 >8
  -\tag #'texts ^\markup {"u"}
  <b\1 fis\2 dis\3 a\4 >4
  -\tag #'texts ^\markup {"d"}
  <e\3 g\2 b\1 >4
  -\tag #'texts ^\markup {"d"}
  -\tag #'chords ^\markup \fret-diagram #"w:4;h:4;1-2;2-3;3-4;4-x;"
  <a\4 c\3 e\2 a\1 >4
  -\tag #'texts ^\markup {"d"}
  -\tag #'chords ^\markup \fret-diagram #"w:4;h:4;1-o;2-o;3-o;4-2;"
  <a\4 dis\3 fis\2 b\1 >4
  -\tag #'texts ^\markup {"d"}
  -\tag #'chords ^\markup \fret-diagram #"w:4;h:4;1-2;2-2;3-3;4-2;"
  r4
  \bar "|."
  \pageBreak
#})
TrackAVoiceBMusic = #(define-music-function (parser location inTab) (boolean?)
#{
#})
TrackALyrics = \lyricmode {
  \set ignoreMelismata = ##t

  \unset ignoreMelismata
}
TrackAStaff = \new Staff <<
  \override StringNumber #'transparent = ##t
  \context Voice = "TrackAVoiceAMusic" {
    \TrackAVoiceAMusic ##f
  }
  \context Voice = "TrackAVoiceBMusic" {
    \TrackAVoiceBMusic ##f
  }
>>
TrackATabStaff = \new TabStaff <<
  \override StringNumber #'transparent = ##t
  \set TabStaff.stringTunings = #custom-tuning
  \context TabVoice = "TrackAVoiceAMusic" {
    \removeWithTag #'chords
    \removeWithTag #'texts
    \TrackAVoiceAMusic ##t
  }
  \context TabVoice = "TrackAVoiceBMusic" {
    \removeWithTag #'chords
    \removeWithTag #'texts
    \TrackAVoiceBMusic ##t
  }
>>
TrackAStaffGroup = \new StaffGroup <<
  \TrackAStaff
  \TrackATabStaff
>>
\score {
  \TrackAStaffGroup
  \header {
    title = "Tout le monde veut devenir un chat"
    copyright="CC BY-SA 4.0"
    composer = \markup \left-column {"Phil Harris" "Scatman Crothers" "Thurl Ravenscroft" "Vito Scotti" "Paul Winchell"}
    instrument = "Ukulele (gCEA)"
    poet="Arranged by Julien Siebert"
  }
  %\midi{}
\layout{}\midi{}
}
