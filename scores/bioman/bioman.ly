#(define (tie::tab-clear-tied-fret-numbers grob)
   (let* ((tied-fret-nr (ly:spanner-bound grob RIGHT)))
      (ly:grob-set-property! tied-fret-nr 'transparent #t)))

\version "2.16.0"
\paper {
   indent = #0
   print-all-headers = ##t
   print-all-headers = ##t
   ragged-right = ##f
   ragged-bottom = ##t
}
\layout {
   \context { \Score
      \override MetronomeMark #'padding = #'5
   }
   \context { \Staff
      \override TimeSignature #'style = #'numbered
      \override StringNumber #'transparent = ##t
   }
   \context { \TabStaff
      \override TimeSignature #'style = #'numbered
      \override Stem #'transparent = ##t
      \override Flag #'transparent = ##t
      \override Beam #'transparent = ##t
      \override Tie  #'after-line-breaking = #tie::tab-clear-tied-fret-numbers
   }
   \context { \TabVoice
      \override Tie #'stencil = ##f
   }
   \context { \StaffGroup
      \consists "Instrument_name_engraver"
   }
}
deadNote = #(define-music-function (parser location note) (ly:music?)
   (set! (ly:music-property note 'tweaks)
      (acons 'stencil ly:note-head::print
         (acons 'glyph-name "2cross"
            (acons 'style 'special
               (ly:music-property note 'tweaks)))))
   note)

palmMute = #(define-music-function (parser location note) (ly:music?)
   (set! (ly:music-property note 'tweaks)
      (acons 'style 'do (ly:music-property note 'tweaks)))
   note)

TrackAVoiceAMusic = #(define-music-function (parser location inTab) (boolean?)
#{
   %\tempo 4=120
   \clef #(if inTab "tab" "treble_8")
   \key c \major
   \time 4/4
   \oneVoice
   <d'\1 a\2 fis\3 d'\4 >4 <a\2 fis\3 >4 <b\1 fis\2 d\3 b\4 >4 <fis\2 d\3 >4
   <a\1 d\3 fis\2 a\4 >4 <fis\2>8 <d\3>8 <fis\2>2
   <d'\1 a\2 fis\3 d'\4 >4 <a\2 fis\3 >4 <b\1 fis\2 d\3 b\4 >4 <fis\2 d\3 >4
   <a\1 fis\2 d\3 a\4 >4 <fis\2>8 <d\3>8 <e\2>2
   <e\2>4 r8 <fis\2 d\3 >8 <e\3 g\2 >2
   <d\3 fis\2 >4 r8 <e\3 g\2 >8 <a\1 fis\2 >2
   <g\2 e\3 >4 <g\2 e\3 >8 <a\1>8 <b\1 g\2 >4 <g\4>4
   <a\1 fis\2 >2 r4 <a\1>4
   <d'\1 a\2 fis\3 d'\4 >4 <fis\3 a\2 >4 r4 <d'\1 a\2 fis\3 d'\4 >4
   <cis'\1 a\2 e\3 cis'\4 >1
   <g\2 e\3 >4 <g\2 e\3 >8 <a\1>8 <b\1 g\2 >4 <g\4>4
   <a\1>2 <fis\2>2
   <e\2>4 r8 <fis\2 d\3 >8 <e\3 g\2 >2
   <d\3 fis\2 >4 r8 <e\3 g\2 >8 <a\1 fis\2 >2
   <g\2 e\3 >4 <g\2 e\3 >8 <a\1>8 <b\1 g\2 >4 <g\4>4
   <a\1 fis\2 >2 r2
   \bar "|."
   \pageBreak
#})
TrackAVoiceBMusic = #(define-music-function (parser location inTab) (boolean?)
#{
#})
TrackALyrics = \lyricmode {
   \set ignoreMelismata = ##t

   \unset ignoreMelismata
}
TrackAStaff = \new Staff <<
   \context Voice = "TrackAVoiceAMusic" {
      \removeWithTag #'chords
      \removeWithTag #'texts
      \TrackAVoiceAMusic ##f
   }
   \context Voice = "TrackAVoiceBMusic" {
      \removeWithTag #'chords
      \removeWithTag #'texts
      \TrackAVoiceBMusic ##f
   }
>>
TrackATabStaff = \new TabStaff \with { stringTunings = #`(,(ly:make-pitch -1 5 NATURAL) ,(ly:make-pitch -1 2 NATURAL) ,(ly:make-pitch -1 0 NATURAL) ,(ly:make-pitch -1 4 NATURAL) ) } <<
   \context TabVoice = "TrackAVoiceAMusic" {
      \removeWithTag #'chords
      \removeWithTag #'texts
      \TrackAVoiceAMusic ##t
   }
   \context TabVoice = "TrackAVoiceBMusic" {
      \removeWithTag #'chords
      \removeWithTag #'texts
      \TrackAVoiceBMusic ##t
   }
>>
TrackAStaffGroup = \new StaffGroup <<
   \TrackAStaff
   \TrackATabStaff
>>
\score {
   \TrackAStaffGroup
   \header {
      title = "Bioman"
      composer = \markup \left-column {"Jean-François Porry", "Gérard Salesses"}
      copyright="CC BY-SA 4.0"
      instrument = "Ukulele (gCEA)"
      poet="Arranged by Julien Siebert"
   }
   %\midi{}
\layout{}\midi{}
}
