#(define (tie::tab-clear-tied-fret-numbers grob)
   (let* ((tied-fret-nr (ly:spanner-bound grob RIGHT)))
      (ly:grob-set-property! tied-fret-nr 'transparent #t)))

\version "2.16.0"
\paper {
   indent = #0
   print-all-headers = ##t
   print-all-headers = ##t
   ragged-right = ##f
   ragged-bottom = ##t
}
\layout {
   \context { \Score
      \override MetronomeMark #'padding = #'5
   }
   \context { \Staff
      \override TimeSignature #'style = #'numbered
      \override StringNumber #'transparent = ##t
   }
   \context { \TabStaff
      \override TimeSignature #'style = #'numbered
      \override Stem #'transparent = ##t
      \override Flag #'transparent = ##t
      \override Beam #'transparent = ##t
      \override Tie  #'after-line-breaking = #tie::tab-clear-tied-fret-numbers
   }
   \context { \TabVoice
      \override Tie #'stencil = ##f
   }
   \context { \StaffGroup
      \consists "Instrument_name_engraver"
   }
}
deadNote = #(define-music-function (parser location note) (ly:music?)
   (set! (ly:music-property note 'tweaks)
      (acons 'stencil ly:note-head::print
         (acons 'glyph-name "2cross"
            (acons 'style 'special
               (ly:music-property note 'tweaks)))))
   note)

palmMute = #(define-music-function (parser location note) (ly:music?)
   (set! (ly:music-property note 'tweaks)
      (acons 'style 'do (ly:music-property note 'tweaks)))
   note)

TrackAVoiceAMusic = #(define-music-function (parser location inTab) (boolean?)
#{
   %\tempo 4=120
   \clef #(if inTab "tab" "treble_8")
   \key c \major
   \time 4/4
   \oneVoice
   \repeat volta 2 {
      <c\3>8 <d\3>8 <dis\3>8 <f\2>8 <g\2 dis\3 >8 <dis\3>8 <g\2>4
      <fis\2 d\3 >8 <d\3>8 <fis\2>4 <f\2 cis\3 >8 <cis\3>8 <f\2>4
      <c\3>8 <d\3>8 <dis\3>8 <f\2>8 <g\2 dis\3 >8 <dis\3>8 <g\2>8 <c'\1>8
      <ais\4>8 <g\2>8 <dis\3>8 <g\2>8 <ais\4>2
   }
   <g\4>8 <a\1>8 <b\4>8 <c'\1>8 <b\4 d'\1 >8 <b\4>8 <d'\1>4
   <dis'\1 b\4 >8 <b\4>8 <dis'\1>4 <d'\1 b\4 >8 <b\4>8 <d'\1>4
   <g\4>8 <a\1>8 <b\4>8 <c'\1>8 <b\4 d'\1 >8 <b\4>8 <d'\1>4
   <dis'\1 b\4 >8 <b\4>8 <dis'\1>4 <d'\1 b\4 >2
   \repeat volta 2 {
      <g\4>8 <a\1>8 <ais\1>8 <c'\1>8 <d'\1 ais\4 >8 <ais\4>8 <d'\1>4
      <cis'\1 a\4 >8 <a\4>8 <cis'\1>4 <c'\1 gis\4 >8 <gis\4>8 <c'\1>4
      <g\4>8 <a\1>8 <ais\1>8 <c'\1>8 <d'\1 ais\4 >8 <ais\4>8 <d'\1>8 <g'\1>8
      <f'\4>8 <d'\2>8 <ais\3>8 <d'\2>8 <f'\4>2
   }
   \bar "|."
   \pageBreak
#})
TrackAVoiceBMusic = #(define-music-function (parser location inTab) (boolean?)
#{
#})
TrackALyrics = \lyricmode {
   \set ignoreMelismata = ##t

   \unset ignoreMelismata
}
TrackAStaff = \new Staff <<
   \context Voice = "TrackAVoiceAMusic" {
      \removeWithTag #'chords
      \removeWithTag #'texts
      \TrackAVoiceAMusic ##f
   }
   \context Voice = "TrackAVoiceBMusic" {
      \removeWithTag #'chords
      \removeWithTag #'texts
      \TrackAVoiceBMusic ##f
   }
>>
TrackATabStaff = \new TabStaff \with { stringTunings = #`(,(ly:make-pitch -1 5 NATURAL) ,(ly:make-pitch -1 2 NATURAL) ,(ly:make-pitch -1 0 NATURAL) ,(ly:make-pitch -1 4 NATURAL) ) } <<
   \context TabVoice = "TrackAVoiceAMusic" {
      \removeWithTag #'chords
      \removeWithTag #'texts
      \TrackAVoiceAMusic ##t
   }
   \context TabVoice = "TrackAVoiceBMusic" {
      \removeWithTag #'chords
      \removeWithTag #'texts
      \TrackAVoiceBMusic ##t
   }
>>
TrackAStaffGroup = \new StaffGroup <<
   \TrackAStaff
   \TrackATabStaff
>>
\score {
   \TrackAStaffGroup
   \header {
      title = "Dans l'antre du roi de la montagne"
      composer = "Edvard Grieg"
      copyright="CC BY-SA 4.0"
      instrument = "Ukulele (gCEA)"
      poet="Arranged by Julien Siebert"
   }
   %\midi{}
\layout{}\midi{}
}
